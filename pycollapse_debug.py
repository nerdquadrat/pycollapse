"""
debug library for pycollapse.py
"""
__AUTHOR__ = "Sven Wagner <pycollapse-647188@sven-markus.blue>" # address may change at any time on arrival of spam
__VERSION__ = "0.1"
__LIBRARY__ = "pycollapse_debug"
__LICENSE__ = "GPLv2" # for details see https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
                      # why GPLv2 ? v3tldr; V3 too long, didn't read
__PROJECT__ = "https://www.gitlab.com/nerdquadrat/pycollapse"

import pycollapse

class ICPdebug(pycollapse.ICP):
    def __init__(self, path, log=None):
        pycollapse.ICP.__init__(self, path, log=log)

    def read_blocks(self, offset, length):
        "read length bytes of raw block data beginning at offset"
        if not self.icpfile.check_block_boundaries(offset, length):
            if self.log:
                self.log.debug("unmatched boundaries")

            return False

        with open (self.icpfile.path, "rb+") as fh:
            fh.seek(offset)

            return fh.read(length)

    def write_blocks(self, offset, raw_data):
        "write length of raw data to blocks beginning at offset, len(raw_data) must be multiple of blocksize ;)"
        if not self.icpfile.check_block_boundaries(offset, len(raw_data)):
            if self.log:
                self.log.debug("unmatched boundaries")

            return False

        with open (self.icpfile.path, "rb+") as fh:
            fh.seek(offset)
            fh.write(raw_data)

    def read_block_10_begin(self, x):
        "read 10 bytes from beginning of block x"
        offset = x * self.icpfile.blocksize

        with open(self.icpfile.path, "rb") as fh:
            fh.seek(offset)
            return(fh.read(10))

    def read_block_10_end(self, x):
        "read last 10 bytes of block nr x"
        offset = x * self.icpfile.blocksize

        with open(self.icpfile.path, "rb") as fh:
            fh.seek(offset + self.icpfile.blocksize - 10)

            return(fh.read(10))

    def write_block_10_begin(self, x, string):
        "write 10 bytes of string to begin of block x"
        offset = x * self.icpfile.blocksize

        with open(self.icpfile.path, "rb+") as fh:
            fh.seek(offset)
            fh.write(string[:10])

    def write_block_10_end(self, x, string):
        "write max of 10 bytes of string to end of block x"
        offset = x * self.icpfile.blocksize
        string = string[:10]

        with open(self.icpfile.path, "rb+") as fh:
            fh.seek(offset + self.icpfile.blocksize - len(string))
            fh.write(string)
