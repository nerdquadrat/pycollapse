"""
This is a library to punch holes into files (make files sparse again!!),
remove/collapse blocks or insert sparse blocks somewhere into the middle
of the file.
Operations of these kernel functions always have to start and end at block
boundaries, thus offset and length must be a multiple of blocksize used
by filesystem.
Some otherwise impossible Operations use truncate() instead of INSERT or
COLLAPSE by default as that is what the user then most likely wants.

By definition, collapse and insert are HIGHLY DESTRUCTIVE for any kind of
file format (shifting the position of 'untouched' data blocks within the
file) and can (or will) additionally cause UNPREDICTABLE BEHAVIOUR on
applications or data when filehandles are help open during operation
of `collapse` or `insert`. `punch` does not change filesize and should be
harmless to the pointer positions of open filehandles.
Collapse and insert seem to be 'sort of safe' for filehandles in append mode,
but NO WARRANTY is given in any way that this library is fit for any purpose
other than 'filling up some diskspace' if you copy this file to your disk ;-)

Think of COLLAPSE, INSERT and PUNCH like beeing a BLACK HOLE in your system,
unlimited amount of data can be sucked in with lightspeed at once, all of it
is immediately lost to the black hole, does not have any size any more, nor
does your data know it has ever existed or has any chance of coming back -
well, you DO have a BACKUP, don't you?
Also black holes bend timespace around them, so do collapse and insert shift
positions of data inside the file and are likely to CAUSE SEVERE HARM to
anything depending on data positions in files. You have been warned.
Also see __LICENSE__

examples:
import pycollapse
c = pycollapse.ICP(path, log=logger)

c.get_blocksize() # returns blocksize of the given file/filesystem

c.punch(4096, 8192) # make 8192 bytes sparse beginning at position 4096

c.insert(4096, 8192) # insert 8192 bytes (two blocks if blocksize is 4k) at
    position of 4096 bytes (if offset is larger or equal filesize, truncate()
    is used) also adding 8192 bytes to the offset of any data with offset
    previously > 4096.

c.collapse(4096, 8192) # remove 8192 bytes from inside the file beginning at
    offset 4096 (if thats larger than the file, truncate is used)
    any data after offset of 4096 is 'moved' 8192 bytes towards beginning of
    the file.

known limitations:

currently only a few filesystems are supported and only in newer versions
(see SUPPORTED_FILESYSTEMS and the check_* functions)

Some older kernel/filesystem versions don't support some features,
calls to check_{punch,insert,collapse} should return False in most cases if
it is known to not work with the given combination,
however false (posi/nega)tives could exist.

For ext3 to ext4 migrated filesystems, some old files could potentially be
left non-extent based and thus will fail as all main actions only work with
extent-based files. I did not figure out how to reliably check a given
file for beeing extent-based or not or if a filesystem was migrated in
the past. I appreciate hints.
However recreating those files (cp -a A B ; mv B A ) should reliably make
them extent-based.

SEEK_DATA and SEEK_HOLE need python >= 3.3 to work,
so do find_next_nonsparse_block() and find_next_sparse_block().
"""

__AUTHOR__ = "Sven Wagner <pycollapse-647188@sven-markus.blue>" # address may change at any time on arrival of spam
__VERSION__ = "0.1"
__LIBRARY__ = "pycollapse"
__LICENSE__ = "GPLv2" # for details see https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
                      # why GPLv2 ? v3tldr; V3 too long, didn't read
__PROJECT__ = "https://www.gitlab.com/nerdquadrat/pycollapse"

# TODO: sanitize return status of functions
# TODO: sanitize logging
# TODO: open file once, persist filehandle and maybe fh.flush() if needed to
#       possibly increase performance on extensive use
# TODO: check files for extent-based block mapping (how to do that?)

# as of http://man7.org/linux/man-pages/man2/fallocate.2.html
# FALLOC_FL_PUNCH_HOLE     is available since Linux 2.6.38
# FALLOC_FL_COLLAPSE_RANGE is available since Linux 3.15 (however 3.10 worked
#                          for me, so check fails for 3.9 or older)
# FALLOC_FL_INSERT_RANGE   is available since Linux 4.1

import os
import sys
import platform
import ctypes
import ctypes.util

F_KEEP_SIZE = 0x01
F_PUNCH_HOLE = 0x02
F_COLLAPSE_RANGE = 0x08
F_INSERT_RANGE = 0x20
C_OFF_T = ctypes.c_int64

SUPPORTED_FILESYSTEMS = [ "ext4", "xfs", "btrfs" ]

STATVFS_F_BSIZE = 0
ST_SIZE = 6

KERNEL_MAJOR, KERNEL_MINOR, KERNEL_PATCH = platform.release().split(".")[:3]
KERNEL_MAJOR = int(KERNEL_MAJOR)
KERNEL_MINOR = int(KERNEL_MINOR)
if "-" in KERNEL_PATCH:
    KERNEL_PATCH = int(KERNEL_PATCH[:KERNEL_PATCH.find("-")])
else:
    KERNEL_PATCH = int(KERNEL_PATCH)

PUNCH = 1
COLLAPSE = 2
INSERT = 3
MODES = { PUNCH: F_KEEP_SIZE | F_PUNCH_HOLE, COLLAPSE: F_COLLAPSE_RANGE, INSERT: F_INSERT_RANGE}
MODENAMES = { PUNCH: "PUNCH", COLLAPSE: "COLLAPSE", INSERT: "INSERT"}

_fallocate = ctypes.CDLL(ctypes.util.find_library('c')).fallocate
_fallocate.restype = ctypes.c_int
_fallocate.argtypes = [ctypes.c_int, ctypes.c_int, C_OFF_T, C_OFF_T]

def fallocate_wrapper(fd, mode, offset, length):
    "fallocate wrapper"
    result = _fallocate(fd.fileno(), mode, offset, length)
    if result != 0:
        raise IOError(result, 'fallocate')

def get_mountpoint_of_file(path, log=False):
    if log: log.debug("get_mountpoint_of_file (file=%s)" % path)

    abspath = os.path.abspath(path)
    original_dev = os.stat(abspath).st_dev

    while abspath != '/':
        directory = os.path.dirname(abspath)

        if os.stat(directory).st_dev != original_dev:
            break

        abspath = directory

    if log:
        log.debug("get_mountpoint_of_file (abspath=%s)" % abspath)

    return abspath

def get_device_info_from_mountpoint(mountpoint, log=False):

    if log:
        log.debug("get_device_info_from_mountpoint (mountpoint=%s)" % mountpoint)

    fs_type = None
    device = None
    params = None

    with open("/proc/mounts", "r") as fh:
        for line in fh:
            data = line.split()

            if data[1] == mountpoint:
                device = data[0]
                fs_type = data[2]
                params = data[3]

                if fs_type != "rootfs":
                    break

    if log:
        log.debug("get_device_info_from_mountpoint (device=%s, fs_type=%s)" % (device, fs_type))

    return (device, fs_type, params)

def check_punch(filesystem, log=False):
    "check if PUNCH should be possible, return True, or False if not"
    if filesystem == "xfs":

        if KERNEL_MAJOR < 2 or (KERNEL_MAJOR == 2 and KERNEL_MINOR < 6) or (KERNEL_MAJOR == 2 and KERNEL_MINOR == 6 and KERNEL_PATCH < 38):

            if log:
                log.debug("kernel version too low (%i.%i) for holepunching (at least 2.6.38 needed for xfs)" % (KERNEL_MAJOR, KERNEL_MINOR))

            return False

    elif filesystem == "ext4":

        if KERNEL_MAJOR < 3:

            if log:
                log.debug("kernel version too low (%i.%i) for holepunching (at least 3.0 needed for ext4)" % (KERNEL_MAJOR, KERNEL_MINOR))

            return False

    elif filesystem == "btrfs":

        if KERNEL_MAJOR < 3 or (KERNEL_MAJOR == 3 and KERNEL_MINOR < 7):

            if log:
                log.debug("kernel version too low (%i.%i) for holepunching (at least 3.7 needed for btrfs)" % (KERNEL_MAJOR, KERNEL_MINOR))

            return False

    else:
        if log:
            log.debug("filesystem '%s' not supported (yet)" % filesystem)

        return False

    if log:
        log.debug("punch_hole should be supported")

    return True

def check_collapse(filesystem, log=False):
    "check if COLLAPSE should be possible, return True, or False if not"
    if filesystem not in ["ext4", "xfs"]:

        if log:
            log.debug("collapse not supported by filesystem")

        return False

    if KERNEL_MAJOR < 3 or ( KERNEL_MAJOR == 3 and KERNEL_MINOR < 10 ):

        if log:
            log.debug("kernel version too low (%i.%i) for collapsing (at least 3.10 needed)" % (KERNEL_MAJOR, KERNEL_MINOR))

        return False

    if log:
        log.debug("collapse should be supported")

    return True

def check_insert(filesystem, log=False):
    "check if INSERT should be possible, return True, or False if not"

    if filesystem == "xfs":

        if KERNEL_MAJOR < 4 or (KERNEL_MAJOR == 4 and KERNEL_MINOR < 1):

            if log:
                log.debug("kernel version too low (%i.%i) for inserting (at least 4.1 needed)" % (KERNEL_MAJOR, KERNEL_MINOR))

            return False

    elif filesystem == "ext4":

        if KERNEL_MAJOR < 4 or (KERNEL_MAJOR == 4 and KERNEL_MINOR < 1):
            if log:
                log.debug("kernel version too low (%i.%i) for inserting (at least 4.1 needed)" % (KERNEL_MAJOR, KERNEL_MINOR))

            return False

    elif filesystem in ["btrfs"]:

        if log:
            log.debug("insert not supported by filesystem")

        return False

    else:
        if log:
            log.debug("insert not supported by filesystem")

        return False

    if log:
        log.debug("insert should be supported")

    return True

class ICPFile(object):
    "file related informations/functions"
    def __init__(self, path, log=None):
        self.path = path
        self.log = log

        self.mountpoint = get_mountpoint_of_file(path, log=log)
        self.device, self.filesystem, self.mount_params = get_device_info_from_mountpoint(self.mountpoint, log=self.log)
        self.blocksize = os.statvfs(self.path)[STATVFS_F_BSIZE]
        if self.log:
            self.log.debug("blocksize: %i" % self.blocksize)

        self.can_punch = check_punch(self.filesystem, log=self.log)
        self.can_collapse = check_collapse(self.filesystem, log=self.log)
        self.can_insert = check_insert(self.filesystem, log=self.log)

    def check_block_boundaries(self, offset, length):
        "check a given offset/length pair to match block boundaries of the file"

        if offset % self.blocksize == 0 and length % self.blocksize == 0:
            return True

        return False

    def get_size(self):
        "return size of file in bytes"
        return os.stat(self.path)[ST_SIZE]

class ICP(object):
    "collapse object ICP stands for (Insert, Collapse, Punch)"
    def __init__(self, path, log=None):
        self.log = log
        self.icpfile = ICPFile(path, self.log)

    def insert(self, offset, length, disable_truncate=False):
        "INSERT length empty blocks into the file at offset"
        if not self.icpfile.can_insert:
            if self.log:
                self.log.error("cannot insert into %s" % self.icpfile.path)

            return False

        if not self.icpfile.check_block_boundaries(offset, length):
            if self.log:
                self.log.warn("insert out of block boundaries not possible (blocksize=%i)" % self.icpfile.blocksize)

            return False

        if offset >= self.icpfile.get_size():

            if disable_truncate:
                if self.log:
                    self.log.debug("inserting sparse blocks at the end of the file is not supported by kernel, aborting due to disable_truncate")

                return False

            if self.log:
                self.log.debug("inserting sparse blocks at the end of the file is not supported by kernel, using 'truncate' instead")
            with open(self.icpfile.path, "rb+") as fh:
                fh.truncate(offset + length)

        else:
            self.do_fcntl(INSERT, offset, length)

    def punch(self, offset, length, disable_truncate=False):
        "make blocks sparse again! (drop data by removing disk allocation, making these blocks sparse)"
        if not self.icpfile.can_punch:
            if self.log:
                self.log.error("cannot punch holes into %s" % self.icpfile.path)

            return False

        if not self.icpfile.check_block_boundaries(offset, length):
            if self.log:
                self.log.warn("punch out of block boundaries not possible (blocksize=%i)" % self.icpfile.blocksize)

            return False

        if offset + length > self.icpfile.get_size():
            if disable_truncate:
                if self.log:
                    self.log.debug("punching holes beyond filesize is not supported by kernel, aborting due to disable_truncate")

                return False

            if self.log:
                self.log.debug("punching hole beyond filesize is not supported by kernel, using truncate before punch")

            with open(self.icpfile.path, "rb+") as fh:
                fh.truncate(offset + length)

        self.do_fcntl(PUNCH, offset, length)

    def collapse(self, offset, length, disable_truncate=False):
        "remove blocks from file, reducing filesize"

        if not self.icpfile.can_collapse:
            if self.log:
                self.log.error("cannot collapse in %s" % self.icpfile.path)

            return False

        if not self.icpfile.check_block_boundaries(offset, length):
            if self.log:
                self.log.error("collapse out of block boundaries not possible (blocksize=%i)" % self.icpfile.blocksize)

            return False

        cur_size = self.icpfile.get_size()

        if offset >= cur_size:
            if self.log:
                self.log.error("collapse begins above filesize, skipping")

            return False

        elif offset + length >= self.icpfile.get_size():

            if disable_truncate:
                if self.log:
                    self.log.debug("collapsing blocks beyond or to EOF is not supported by kernel, aborting due to disable_truncate")

                return False

            if self.log:
                self.log.debug("collapse beyond or to EOF is not supported by kernel, using truncate instead")

            with open(self.icpfile.path, "rb+") as fh:
                fh.truncate(offset)

        else:
            self.do_fcntl(COLLAPSE, offset, length)

    def do_fcntl(self, mode, offset, length):
        "internal function, but if you know what you do..."
        if self.log:
            self.log.debug("%s blocks: %i bytes from %i to %i" % (MODENAMES[mode], length, offset, offset + length))

        with open(self.icpfile.path, "r+") as fh:
            fallocate_wrapper(fh, MODES[mode], offset, length)

#    def zero_to_sparse(self, offset, length):
#         # TODO create function to punch holes where only zeroes are

    def get_next_block_start(self, offset):
        "from given offset (does not need to match block boundaries) get the next blocks starting offset"
        if offset % self.icpfile.blocksize == 0:
            return offset

        return offset + (self.icpfile.blocksize - offset % self.icpfile.blocksize)

    def get_block_start(self, offset):
        "get the starting offset of the block the given offset is in"
        return offset - offset % self.icpfile.blocksize

    def get_previous_block_start(self, offset):
        "from given offset, get the previous blocks (not including given offset) starting offset"
        return offset - offset % self.icpfile.blocksize - self.icpfile.blocksize

    def is_block_sparse(self, offset):
        "return true if block at offset is sparse, false if not"
        # TODO ensure sane return values on
        # - offset is >= EOF
        # - no non-sparse block present until EOF
        if find_next_nonsparse_block(offset) == offset:
            return True

        return False

    def find_next_nonsparse_block(self, offset):
        """
        resourcefriendly way to walk through gigabytes of sparse block
        zeroes, finding the start offset of next allocated block.
        That block might consist of only zeroes, but it is allocated and thus
        NOT sparse.
        given offset does not need to be at block boundaries in this function
        and can result in the given offset to be returned back, if the block
        at that offset is allocated.
        """
        if sys.version_info[0] < 3 or (sys.version_info[0] == 3 and sys.version_info[1] < 3):
            if self.log:
                self.log.error("python version to small, >= 3.3 needed")

            return False

        with open (self.icpfile.path, "rb+") as fh:
            return os.lseek(fh.fileno(), offset, os.SEEK_DATA)

    def find_next_sparse_block(self, offset):
        """
        resourcefriendly way to walk through gigabytes of allocated data blocks,
        finding the start offset of the next sparse block
        offset does not need to be at block boundaries here and can result
        in the given offset to be returned back if that block is sparse.
        """
        if sys.version_info[0] < 3 or (sys.version_info[0] == 3 and sys.version_info[1] < 3):
            if self.log:
                self.log.error("python version to small, >= 3.3 needed")

            return False

        with open (self.icpfile.path, "rb+") as fh:
            return os.lseek(fh.fileno(), offset, os.SEEK_HOLE)

    def get_blocksize(self):
        "get blocksize of file (really blocksie of underlying filesystem)"
        return self.icpfile.blocksize
