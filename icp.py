#!/usr/bin/python
"""
ICP stands for Insert Collapse and Punch
(make files sparse again!!)

this is a script to make use of pycollapse from command line / shell scripts

please read comment in pycollapse.py and USE this script WITH CARE and on your own risk!!!
some features are DESTRUCTIVE by definition ;-) and NO WARRANTY is given. You have been warned!

examples:
# insert 4096 bytes (1 sparse block) at the beginning of /path/to/file
icp.py -f /path/to/file -I -o 0 -l 4096
"""

__AUTHOR__ = "Sven Wagner <pycollapse-647188@sven-markus.blue>" # address may change at any time on arrival of spam
__VERSION__ = "0.1"
__SCRIPT__ = "icp.py"
__LICENSE__ = "GPLv2" # for details see https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
                      # why GPLv2 ? v3tldr; V3 too long, didn't read
__PROJECT__ = "https://www.gitlab.com/nerdquadrat/pycollapse"

import argparse
import pycollapse

argParser = argparse.ArgumentParser(description='Script to use pycollapse(_debug)? and punch holes into (extent-based) files or insert/collapse block ranges',
                                    epilog='Licensed under %s, send bug reports to %s or open issues/pull requests to the project on %s' % (__LICENSE__, __AUTHOR__, __PROJECT__))


argParser.add_argument("-f", "--file", help="file to work on", required=True)
argParser.add_argument("-o", "--offset", help="offset to start from (must match block boundary for punch, insert or collapse)", type=int)
argParser.add_argument("-l", "--length", help="length", type=int)

argParser.add_argument("-P", "--punch", help="PUNCH mode", action="store_true")
argParser.add_argument("-I", "--insert", help="INSERT mode", action="store_true")
argParser.add_argument("-C", "--collapse", help="COLLAPSE mode", action="store_true")

argParser.add_argument("-t", "--disable-truncate", help="do not use truncate to operate beyond/to EOF", action="store_true")

argParser.add_argument("-L", "--log", help="activate logging", action="store_true")
argParser.add_argument("-D", "--debug", help="activate debug logging", action="store_true")

argParser.add_argument("--get-blocksize", help="get blocksize of file", action="store_true")
argParser.add_argument("--check-punch", help="check if PUNCH_HOLE should work, otherwise exit with errorcode 1", action="store_true")
argParser.add_argument("--check-collapse", help="check if COLLAPSE should work, otherwise exit with errorcode 1", action="store_true")
argParser.add_argument("--check-insert", help="check if INSERT should work, otherwise exit with errorcode 1", action="store_true")

argParser.add_argument("--find-next-sparse-block", help="resourcefriendly find the next sparse block beginning at offset", action="store_true")
argParser.add_argument("--find-next-nonsparse-block", help="resourcefriendly find the next allocated (nonsparse) block", action="store_true")

argParser.add_argument("--get-block-start", help="get the starting offset of current block (that includes the given offset)", action="store_true")
argParser.add_argument("--get-next-block-start", help="get the starting offset of the block following the current block (that includes offset)", action="store_true")
argParser.add_argument("--get-previous-block-start", help="get the starting offset of the block before the current block (that includes offset)", action="store_true")

argParser.add_argument("--get-device-info", help="get some infos about the filesystem the file resides on", action="store_true")
argParser.add_argument("--get-mountpoint", help="get mountpoint of filesystem the given file resides on", action="store_true")

argParser.add_argument("--write-block-10-begin", help="debug function, write up to 10 bytes at beginning of block 'offset' (block numbers, NOT bytes here)")
argParser.add_argument("--write-block-10-end", help="debug function, write up to 10 bytes at end of block 'offset' (block numbers, NOT bytes here)")
argParser.add_argument("--read-block-10-begin", help="debug function, read 10 bytes at start of block 'offset' (block numbers, NOT bytes here)", action="store_true")
argParser.add_argument("--read-block-10-end", help="debug function, read 10 bytes at end of block 'offset' (block numbers, NOT bytes here)", action="store_true")

argParser.add_argument("-V", "--version", help="display script version and exit", action="version", version = __SCRIPT__ + ' ' + __VERSION__)
flags = argParser.parse_args()

if flags.log or flags.debug:
    import logging

    if flags.debug:
        logging.basicConfig(level=logging.DEBUG)

    else:
        logging.basicConfig(level=logging.INFO)

    log = logging.getLogger(__name__)

else:
    log = None

if flags.write_block_10_begin or flags.write_block_10_end or flags.read_block_10_begin or flags.read_block_10_end:
    import pycollapse_debug
    c = pycollapse_debug.ICPdebug(flags.file, log=log)
else:
    c = pycollapse.ICP(flags.file, log=log)

if flags.write_block_10_begin:
    c.write_block_10_begin(flags.offset, flags.write_block_10_begin)

elif flags.write_block_10_end:
    c.write_block_10_end(flags.offset, flags.write_block_10_end)

elif flags.read_block_10_begin:
    c.read_block_10_begin(flags.offset)

elif flags.read_block_10_end:
    c.read_block_10_end(flags.offset)

elif flags.punch and (flags.offset or flags.offset == 0) and flags.length:
    c.punch(flags.offset, flags.length, disable_truncate=flags.disable_truncate)

elif flags.insert and (flags.offset or flags.offset == 0) and flags.length:
    c.insert(flags.offset, flags.length, disable_truncate=flags.disable_truncate)

elif flags.collapse and (flags.offset or flags.offset == 0) and flags.length:
    c.collapse(flags.offset, flags.length, disable_truncate=flags.disable_truncate)

elif flags.get_blocksize:
    print(c.icpfile.blocksize)

elif flags.check_punch:
    print(c.icpfile.can_punch)

elif flags.check_collapse:
    print(c.icpfile.can_collapse)

elif flags.check_insert:
    print(c.icpfile.can_insert)

elif flags.find_next_sparse_block and flags.offset:
    print(c.find_next_sparse_block(flags.offset))

elif flags.find_next_nonsparse_block and flags.offset:
    print(c.find_next_nonsparse_block(flags.offset))

elif flags.get_block_start and flags.offset:
    print(c.get_block_start(flags.offset))

elif flags.get_next_block_start and flags.offset:
    print(c.get_next_block_start(flags.offset))

elif flags.get_previous_block_start and flags.offset:
    print(c.get_previous_block_start(flags.offset))

elif flags.get_device_info:
    print("device=%s, filesystem=%s, params=%s" % (c.icpfile.device, c.icpfile.filesystem, c.icpfile.mount_params ))

elif flags.get_mountpoint:
    print(c.icpfile.mountpoint)

else:
    argParser.print_help()
