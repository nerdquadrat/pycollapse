#!/usr/bin/python3
"""
make use of copy_collapse.py and pycollapse.py to copy-collapse (instead of
copy-truncate) a file to i.e. in-place 'rotate' a logfile that is opened in
append mode by another process that cannot not be restarted for logrotation
and it's designers seem too noob/inexperienced or at least too lazy to
implement simple re-opening of logfiles on a HUP signal or such stuff.
1000 thanks to the so crazycool(!) kernel developers for implementing PUNCH
and COLLAPSE and thus indirect workaround-rescueing these broken-designed
daemon's logging :)

see __LICENCE__ and remember this library comes WITHOUT ANY WARRANTY
"""

import sys
import time
import argparse
import copy_collapse

__AUTHOR__ = "Sven Wagner <pycollapse-647188@sven-markus.blue>" # address may change at any time on arrival of spam
__VERSION__ = "0.1"
__SCRIPT__ = "copycollapse.py"
__LICENSE__ = "GPLv2" # for details see https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
                      # why GPLv2 ? v3tldr; V3 too long, didn't read
__PROJECT__ = "https://www.gitlab.com/nerdquadrat/pycollapse"

TIME_SLEEP = 15

LOGFILE = "/tmp/copy-collapse.log"

if __name__ == '__main__':
    argParser = argparse.ArgumentParser(description='Script to use pycollapse to copy/collapse (instead of copy/truncate a logfile and in-place rotate it while it is opened in append mode by other process (that must not be restarted for logrotation)',
                                        epilog='Licensed under %s, send bug reports to %s or open issues/pull requests to the project on %s' % (__LICENSE__, __AUTHOR__, __PROJECT__))


    argParser.add_argument("-f", "--file", help="file to work on", required=True)
    argParser.add_argument("-O", "--outfile", help="file to work on", required=True)
    argParser.add_argument("-t", "--time", help="seconds to sleep after getting filesize (let log readers follow up to that position (defaults to %i)" % TIME_SLEEP, type=int, default=TIME_SLEEP)

    argParser.add_argument("-L", "--log", help="activate logging", action="store_true")
    argParser.add_argument("--logfile", help="logfile, defaults to %s" % LOGFILE, default=LOGFILE)
    argParser.add_argument("-D", "--debug", help="activate debug logging", action="store_true")

    argParser.add_argument("-V", "--version", help="display script version and exit", action="version", version = __SCRIPT__ + ' ' + __VERSION__)
    flags = argParser.parse_args()

    if flags.log or flags.debug:
        import logging

        if flags.debug:
            logging.basicConfig(filename=flags.logfile,
                                filemode='a',
                                format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                                datefmt='%H:%M:%S',
                                level=logging.DEBUG)

        else:
            logging.basicConfig(filename=flags.logfile,
                                filemode='a',
                                format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                                datefmt='%H:%M:%S',
                                level=logging.INFO)

        log = logging.getLogger(__name__)

    else:
        log = None

    if log:
        log.info("INIT %s" % " ".join(sys.argv))

    obj = copy_collapse.copyCollapse(flags.file, flags.outfile, log=log)

    if flags.time > 0:
        if log:
            log.info("waiting %i seconds for external programs." % flags.time)

        time.sleep(flags.time)

    obj.rotate()

    if log:
        log.info("done")
        #log.close()
