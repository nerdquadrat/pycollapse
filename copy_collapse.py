#!/usr/bin/python3
"""
make use of pycollapse.py to copy-collapse (instead of copy-truncate) a file
to i.e. in-place 'rotate' a logfile that is opened in append mode by another
process that cannot not be restarted for logrotation and it's designers seem
too noob/inexperienced or at least too lazy to implement simple re-opening of
logfiles on a HUP signal or such stuff.
1000 thanks to the so crazycool(!) kernel developers for implementing PUNCH
and COLLAPSE and thus indirect workaround-rescueing these broken-designed
daemon's logging :)

see __LICENCE__ and remember this library comes WITHOUT ANY WARRANTY
"""

import os
import sys
import pycollapse

__AUTHOR__ = "Sven Wagner <pycollapse-647188@sven-markus.blue>" # address may change at any time on arrival of spam
__VERSION__ = "0.1"
__LIBRARY__ = "copy_collapse.py"
__LICENSE__ = "GPLv2" # for details see https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
                      # why GPLv2 ? v3tldr; V3 too long, didn't read
__PROJECT__ = "https://www.gitlab.com/nerdquadrat/pycollapse"

MAX_LINE_LENGTH = 128 * 1024
TIME_SLEEP = 15

ZEROBYTE = b'\x00'
LINEBREAK = b'\n'

class copyCollapse(object):
    def __init__(self, in_file, out_file, log=None):
        self.in_file = in_file
        self.out_file = out_file

        self.fh_in = open(in_file, "rb+")
        self.fh_out = open(out_file, "ab")

        self.log = log

        self.fh_in.seek(0, os.SEEK_END)
        self.filesize = self.fh_in.tell()

        self.data_size = 0

    def check_collapse_support(self):
        "check if punch, collapse and seek nonsparse are supported"
        if not self.icp.icpfile.can_punch:
            if self.log:
                self.log.error("punch not supported")

            return False

        if not self.icp.icpfile.can_collapse:
            if self.log:
                self.log.error("collapse not supported")

            return False

        if sys.version_info[0] < 3 or (sys.version_info[0] == 3 and sys.version_info[1] < 3):
            if self.log:
                self.log.error("python version to small, >= 3.3 needed")

            return False

        return True

    def find_last_newline_pos(self):
        "in case of unfinished writes, try to find the last newline before EOF"
        if self.log:
            self.log.debug("finding last newline")

        if self.filesize < MAX_LINE_LENGTH:
            self.fh_in.seek(0, os.SEEK_SET)

        else:
            self.fh_in.seek(- MAX_LINE_LENGTH, os.SEEK_END)

        byte=" "

        last_linebreak = -1

        while self.fh_in.tell() < self.filesize:

            byte = self.fh_in.read(1)
            if byte == LINEBREAK:
                last_linebreak = self.fh_in.tell()

        if last_linebreak > 0:
            if self.log: self.log.debug("found last linebreak at %i" % last_linebreak)
            self.data_size = last_linebreak

        else:
            self.data_size = self.fh_in.tell()
            if self.log:
                self.log.debug("no last linebreak found, using data_size %i" % self.data_size)

    def get_first_byte_position(self):

        pos = self.icp.find_next_nonsparse_block(0)

        if self.log:
            self.log.debug("block_size: %i" % self.block_size)
            self.log.debug("beginning at first non-sparse block: %i" % pos)

        self.fh_in.seek(pos, os.SEEK_SET)
        byte = self.fh_in.read(1)

        while byte == ZEROBYTE or byte == LINEBREAK:
            byte = self.fh_in.read(1)

        if self.fh_in.tell() == self.data_size:
            return self.fh_in.tell()

        pos = self.fh_in.tell() - 1
        self.fh_in.seek(pos, os.SEEK_SET)
        if self.log:
            self.log.debug("first position of byte to read: %i" % pos)

        return pos

    def copy_first_block_remainder(self):
        blocks = int(self.first_byte_pos / self.block_size)

        remaining_to_block_boundary = self.block_size * ( blocks + 1 ) - (self.first_byte_pos - blocks * self.block_size)

        if self.log:
            self.log.debug("block remainder: %i" % remaining_to_block_boundary)

        if self.first_byte_pos + remaining_to_block_boundary >= self.data_size:
            if self.log:
                self.log.debug("end of data_size: only %i bytes to handle" % (self.data_size - self.first_byte_pos))

            self.fh_out.write(self.fh_in.read(self.data_size - self.first_byte_pos))
            self.fh_out.flush()
            self.fh_in.seek(self.first_byte_pos, os.SEEK_SET)
            self.fh_in.write(ZEROBYTE * (self.data_size - self.first_byte_pos))
            self.fh_in.flush()

            return

        self.fh_out.write(self.fh_in.read(remaining_to_block_boundary))
        self.fh_out.flush()

        blocks_to_punch = int(self.fh_in.tell() / self.block_size)
        if blocks_to_punch > 0:
            if self.log:
                self.log.debug("punching %i blocks" % blocks_to_punch)

            self.icp.punch(0, blocks_to_punch * self.block_size)

    def rotate(self):
        self.fh_in.seek(self.filesize - 1)

        byte = self.fh_in.read(1)

        if byte != LINEBREAK:
            self.find_last_newline_pos()

        else:
            if self.log:
                self.log.debug("last byte is newline, data_size: %i" % self.filesize)
            self.data_size = self.filesize

        if self.data_size == 0:
            if self.log:
                self.log.info("filesize or data_size is 0, nothing to do")

            return True

        self.icp = pycollapse.ICP(self.in_file, log=self.log)

        if not self.check_collapse_support():
            return False

        self.block_size = self.icp.get_blocksize()

        self.first_byte_pos = self.get_first_byte_position()

        if self.first_byte_pos == self.data_size:
            if self.log:
                self.log.info("no new data to copy")

            return True

        self.copy_first_block_remainder()

        if self.fh_in.tell() == self.data_size:
            if self.log:
                self.log.info("done copying first blocks data, no complete block")

            return True

        if self.fh_in.tell() % self.block_size != 0:
            raise Exception("BUG1 %i %i %i" % (self.fh_in.tell(), self.block_size, self.data_size))

        self.copy_flush_blocks()
        self.copy_last_block()
        self.collapse_first_empty_blocks()

        return True

    def copy_flush_blocks(self):
        flush_round = 0
        flush_blocks = int((1024 * 1024) / self.block_size) # flush-punch every MB

        last_flush_pos = self.fh_in.tell()

        while self.fh_in.tell() + self.block_size <= self.data_size:
            self.fh_out.write(self.fh_in.read(self.block_size))

            flush_round += 1
            if flush_round % flush_blocks == 0:

                flush_round = 0

                self.fh_out.flush()

                self.icp.punch(last_flush_pos, self.block_size * flush_blocks)
                self.fh_in.flush()

                last_flush_pos = self.fh_in.tell()


        self.fh_out.flush()

        if flush_round > 0:
            self.icp.punch(last_flush_pos, self.block_size * flush_round)

    def copy_last_block(self):
        pos = self.fh_in.tell()

        if pos != self.data_size:
            n = self.data_size % self.block_size
            if self.log:
                self.log.debug("handling (copy/zeroing) last %i bytes" % n)
            self.fh_out.write(self.fh_in.read(n))
            self.fh_in.seek(pos, os.SEEK_SET)
            self.fh_in.write(ZEROBYTE * n)

        self.fh_out.flush()
        self.fh_in.flush()

    def collapse_first_empty_blocks(self):
        data_begin = self.icp.find_next_nonsparse_block(0)

        if data_begin > 0:
            if self.log:
                self.log.debug("collapsing first %i empty bytes of file" % data_begin)

            self.icp.collapse(0, data_begin)
